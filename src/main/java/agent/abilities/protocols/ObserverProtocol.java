/**
 * 
 */
package agent.abilities.protocols;

//import instantiation.edens.MyOntology;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import FIPA.Envelope;
import agent.AbstractDeltaAgent;
import agent.abilities.Ability;
import agent.abilities.protocols.MyOntology;
//import platform.agents.AAgent;
//import platform.agents.agent.behaviours.ShowKnowledge;
//import platform.agents.agent.knowledge.observer.ObservableK;
//import platform.agents.agent.knowledge.observer.ObserverK;
import debug.*;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SimpleBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import knowledge.Knowledge;
import knowledge.ObservableK;

//import tools.Debug;




/**
 * Observer									Observable
 * 
 * Send    -------- Subscribe ------------>	Receive
 * Receive <-------- Update ---------------	Send
 * Send    -------- UnSubscribe ---------->	Receive
 * 
 * This class intend to be a generic implementation of the design pattern observer.
 * The agents know what to update using the protocolID information available in the messages.
 * Thus, if you want a specific observer, you just have to create your protocolID in the observer-ontology
 * and to implement a specific knowledge class that will be given in parameter of the ObserverProtocolK class.
 * 
 * @author CH217288
 *
 */

/**
 * Plantuml generate automatically a png from this file :
 * 
 @startuml
Observer -> Observable : Subscribe <SpecificData>
Observable ->Observer : Inform<ProductionPlan>
Observer -> Observable : UnSubscribe <SpecificData>
@enduml
 *
 */
public class ObserverProtocol implements Ability{



	/************************************************************************************
	 * 
	 * 									Observer's role 
	 *
	 ************************************************************************************/

	class R1_SendSubscribeObservableRequest extends OneShotBehaviour {

		/**
		 * Used to subscribe as an observer of several components
		 */

		private static final long serialVersionUID = -6455240076287411777L;
		private String observedElementId;
		private String conversationId;
		private List<String> observableAgents;

		/**
		 * Used to subscribe as an observer to several observable components
		 * @param a this
		 * @param ObservedElementID the id of the observed element in the observer-protocol ontonlogy
		 * @param observableAgents the list of agents that possess this element and that I want to subscribe to (the results received will be agregated)
		 * @param conversationId Used to allow the possibility for the observer to observe the same thing from 2 different sources and still discriminating them
		 */
		public R1_SendSubscribeObservableRequest(Agent a,String ObservedElementID,List<String> observableAgents,String conversationId) {		
			super(a);
			this.observedElementId=ObservedElementID;
			this.observableAgents=observableAgents;
			this.conversationId=conversationId;
		}

		@Override
		public void action() {


			ACLMessage msg=new ACLMessage(ACLMessage.SUBSCRIBE);
			msg.setLanguage(MyOntology.LANGUAGE);
			msg.setOntology(MyOntology.ONTOLOGY_PROTO_OBSERVER);
			msg.setProtocol(this.observedElementId);// the specific type of subscription
			msg.setConversationId(this.conversationId);

			msg.setSender(this.myAgent.getAID());
			for(String agentName:observableAgents){
				msg.addReceiver(new AID(agentName,AID.ISLOCALNAME));
			};

			super.myAgent.send(msg);
			//Debug.sniffSend(msg,5);

		}


	}//end subscribeObservableBehaviour


	class R1_SendUnsubscribeObservableRequest extends OneShotBehaviour {

		/**
		 * Used to subscribe as an observer of several components
		 */

		private static final long serialVersionUID = -6455240076287411777L;

		private String observedElementId;
		private List<String> observableAgents;
		private String conversationID;

		/**
		 * Used to unsubscribe as an observer to several components
		 * @param a this
		 * @param observedElementId the Observer protocol definition in the ontology
		 * @param observableAgents the list of agents that agent a want to unsubscribe to
		 * 
		 */
		public R1_SendUnsubscribeObservableRequest(Agent a,String observedElementId,List<String> observableAgents,String conversationId) {		
			super(a);
			this.observedElementId=observedElementId;
			this.observableAgents=observableAgents;
			this.conversationID=conversationId;
		}

		@Override
		public void action() {

			ACLMessage msg=new ACLMessage(ACLMessage.CANCEL);
			msg.setLanguage(MyOntology.LANGUAGE);
			msg.setOntology(MyOntology.ONTOLOGY_PROTO_OBSERVER);
			msg.setProtocol(this.observedElementId);
			msg.setConversationId(this.conversationID);

			msg.setSender(this.myAgent.getAID());
			for(String agentName:observableAgents){
				msg.addReceiver(new AID(agentName,AID.ISLOCALNAME));
			};

			super.myAgent.send(msg);
		}

	}


	class R1_ReceiveUpdateNotification extends SimpleBehaviour{

		private static final long serialVersionUID = -6728022543797605219L;

		/**
		 *Behaviour allowing an observer agent to update its informations regarding an observable one
		 * @param a this
		 * 
		 */
		public R1_ReceiveUpdateNotification(Agent a){
			super(a);
		}

		@Override
		public void action() {

			// build the good template (filter of messages)
			final MessageTemplate perfTmplt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
			final MessageTemplate langTmplt = MessageTemplate.MatchLanguage(MyOntology.LANGUAGE);
			final MessageTemplate ontoTmplt = MessageTemplate.MatchOntology(MyOntology.ONTOLOGY_PROTO_OBSERVER);

			final MessageTemplate totalTmplt = MessageTemplate.and(MessageTemplate.and(perfTmplt, langTmplt),ontoTmplt);

			//I do not check the conversation ID or the protocolId has they will be used to trigger the right cast after
			// try to get the message (if it cross the filter)
			final ACLMessage msg = super.myAgent.receive(totalTmplt);


			if (msg != null) {
				//Debug.sniffReception(msg,5);
				//the protocol indicates the type of information that the sender asked for, and the conversationId (optional) rafines it
				Debug.info(this.getClass(),this.myAgent.getLocalName()+ " must update its knowledge from " + msg.getSender().getLocalName()+" for the info: "+msg.getProtocol(),5);

				//The conversationID and the protocolId form the key to get the knowldge component of the local agent that knows how to handle the content of the message
				Knowledge k= ((AbstractDeltaAgent)super.myAgent).getKnowledge(msg.getProtocol(),msg.getConversationId());
				k.updateK(msg);
				//super.myAgent.addBehaviour(new ShowKnowledge(super.myAgent,msg.getSender().getLocalName()));
			} else {
				block();
			}


		}


		@Override
		public boolean done() {

			return false;
		}

	}




	/************************************************************************************
	 * 
	 * 									Observable's role
	 *
	 ************************************************************************************/


	class R2_ReceiveSubscriptionRequest extends SimpleBehaviour{

		private static final long serialVersionUID = -1316436476566735954L;

		/**
		 *Behaviour allowing an observable agent to add an observer to its list
		 * @param a this
		 * 
		 */
		public R2_ReceiveSubscriptionRequest(Agent a){
			super(a);
		}

		@Override
		public void action() {

			// build the good template (filter of messages)
			final MessageTemplate perfTmplt = MessageTemplate.MatchPerformative(ACLMessage.SUBSCRIBE);
			final MessageTemplate langTmplt = MessageTemplate.MatchLanguage(MyOntology.LANGUAGE);
			final MessageTemplate ontoTmplt = MessageTemplate.MatchOntology(MyOntology.ONTOLOGY_PROTO_OBSERVER);

			final MessageTemplate totalTmplt = MessageTemplate.and(MessageTemplate.and(perfTmplt, langTmplt),ontoTmplt);

			// try to get the message (if it cross the filter)
			final ACLMessage msg = super.myAgent.receive(totalTmplt);


			if (msg != null) {
				//Debug.sniffReception(msg,5);
				//the protocol indicates the type of information that the sender asked for
				Debug.info(this.getClass(),this.myAgent.getLocalName()+ " must add" + msg.getSender()+"for the info: "+msg.getProtocol(),5);
				//assumes that the receiver agent already know that this element (id with protocol+conversation) is observable
				//TODO in the future preferable to answer no than to do noting (or assumes that everything is observable, but i do not agree with this perception)
				ObservableK k= (ObservableK)((AbstractDeltaAgent)super.myAgent).getKnowledge(msg.getProtocol(),msg.getConversationId());
				k.addObserver(msg.getSender());

			} else {
				block();
			}

		}


		@Override
		public boolean done() {
			return false;
		}

	}



	class R2_ReceiveUnSubscriptionRequest extends SimpleBehaviour{

		private static final long serialVersionUID = -1316436476566735954L;

		/**
		 *Behaviour allowing an observable agent to remove an observer from its list
		 * @param a this
		 * 
		 */
		public R2_ReceiveUnSubscriptionRequest(Agent a){
			super(a);
		}

		@Override
		public void action() {

			// build the good template (filter of messages)
			final MessageTemplate perfTmplt = MessageTemplate.MatchPerformative(ACLMessage.CANCEL);
			final MessageTemplate langTmplt = MessageTemplate.MatchLanguage(MyOntology.LANGUAGE);
			final MessageTemplate ontoTmplt = MessageTemplate.MatchOntology(MyOntology.ONTOLOGY_PROTO_OBSERVER);

			final MessageTemplate totalTmplt = MessageTemplate.and(MessageTemplate.and(perfTmplt, langTmplt),ontoTmplt);

			// try to get the message (if it cross the filter)
			final ACLMessage msg = super.myAgent.receive(totalTmplt);

			if (msg != null) {
				//the protocol indicates the type of information that the sender asked for
				Debug.info(this.getClass(), this.myAgent.getLocalName()+ " must remove" + msg.getSender()+"for the info: "+msg.getProtocol(),5);
				ObservableK k= (ObservableK)((AbstractDeltaAgent)super.myAgent).getKnowledge(msg.getProtocol(),msg.getConversationId());
				k.removeObserver(msg.getSender());

			} else {
				block();
			}


		}


		@Override
		public boolean done() {

			return false;
		}

	}



	class R2_SendUpdateNotification extends OneShotBehaviour{

		private static final long serialVersionUID = 3615986712596643781L;
		
		private String protocolId;
		private String conversationId;

		/**
		 *Behaviour allowing an observable agent to send the information associated to a given observedValueId to its subscribers
		 * @param a this
		 * @param observedValueId id key of the information to send (this key is specific to a given type of observable)
		 * @param conversationId Used to differentiate the potentially various instances of a given protocol
		 * 
		 */
		public R2_SendUpdateNotification(Agent a,String observedValueId,String conversationId){
			super(a);
			this.conversationId=conversationId;
			this.protocolId=observedValueId;
		}


		@Override
		public void action() {

			ACLMessage msg=new ACLMessage(ACLMessage.INFORM);
			msg.setLanguage(MyOntology.LANGUAGE);
			msg.setOntology(MyOntology.ONTOLOGY_PROTO_OBSERVER);
			msg.setSender(this.myAgent.getAID());

			msg.setProtocol(this.protocolId);// the specific type of subscription
			msg.setConversationId(this.conversationId);

			ObservableK goK=null;
			try {
				//msg.setContentObject(((AAgent)super.myAgent).getUpdatedData(this.protocolId));
				goK=(ObservableK) ((AbstractDeltaAgent)super.myAgent).getKnowledge(protocolId,conversationId);
				//System.out.println("Test: ");
				//System.out.println("Class:" +goK.getClass()+ " toStr: "+goK.toString());
				msg.setContentObject(goK.getPublicData(super.myAgent));


			} catch (IOException e) {
				System.out.println(this.getClass()+" Crash");
				e.printStackTrace();
			}

			//get all the observer agents
			List<AID> agentAIDList= (goK!=null) ? goK.getObservers(this.protocolId):null;
			if (agentAIDList!=null && !agentAIDList.isEmpty()){
				for(AID agentAID:agentAIDList){
					msg.addReceiver(agentAID);
				};
				super.myAgent.send(msg);
			}
		}

	}



	/************************************************************************************
	 * 
	 * 									Ability Interface
	 *
	 *addAbility(
		 	new ObserverProtocol(), //Instance de la capacité
		 	MyOntology.PROTO_OBSERVER_ROLE_OBSERVABLE,//Rôle de l’agent, ici observable
		 	MyOntology.PROTO_OBSERVER_ID_CURRENTCONSUMPTION,//Elément observé
		   	new ObservableK(new SpecificObservableK()),//Brique de connaissance dédiée
		 	l //éventuels paramètres 
		);
	 ************************************************************************************/


	/**
	 * 
	 */
	@Override
	public List<Behaviour> getBehaviours(String role,List<List<Object>> protocolParameters) {
		List<Behaviour> lb=new ArrayList<Behaviour>();

		switch (role) {
		case MyOntology.PROTO_OBSERVER_ROLE_OBSERVER :
			//the alternative is to use the generic callMethod that use introspection (tools.GenericCall)
			lb.add(new R1_SendSubscribeObservableRequest((Agent)protocolParameters.get(0).get(0), (String)protocolParameters.get(0).get(1),(List<String>) protocolParameters.get(0).get(2),(String)protocolParameters.get(0).get(3)));
			//not at the beginning
			//lb.add(new SendUnsubscribeObservableRequest((Agent)protocolParameters.get(1).get(0), (String)protocolParameters.get(1).get(1),(List<String>) protocolParameters.get(1).get(2)));
			lb.add(new R1_ReceiveUpdateNotification((Agent)protocolParameters.get(1).get(0)));
			break;
		case MyOntology.PROTO_OBSERVER_ROLE_OBSERVABLE:
			lb.add(new R2_ReceiveSubscriptionRequest((Agent)protocolParameters.get(0).get(0)));
			lb.add(new R2_ReceiveUnSubscriptionRequest((Agent)protocolParameters.get(1).get(0)));
			lb.add(new R2_SendUpdateNotification((Agent)protocolParameters.get(2).get(0),(String)protocolParameters.get(2).get(1),(String)protocolParameters.get(2).get(2)));
			break;
		default :
			System.out.println("Observer protocol -- getBehaviours -- unknown role: "+role);
		}
		return lb;
	}


}
