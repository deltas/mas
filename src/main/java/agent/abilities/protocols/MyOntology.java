package agent.abilities.protocols;

public class MyOntology {

	public static final String LANGUAGE="Lang-Delta";
	
	public static final String ONTOLOGY_PROTO_OBSERVER = "Proto-Observer";
	public static final String PROTO_OBSERVER_ROLE_OBSERVER ="Observer";
	public static final String PROTO_OBSERVER_ROLE_OBSERVABLE ="Observable";
	
	public static final String ONTOLOGY_PROTO_SUICIDE = "Proto-Suicide";
	public static final String PROTO_SUICIDE = "Proto-Suicide";
	public static final String PROTO_SUICIDE_ROLE_KILLER = "Killer";
	public static final String PROTO_SUICIDE_ROLE_KILLED = "Killed";
	
	
}
