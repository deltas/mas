package agent.abilities.protocols;

import java.util.ArrayList;
import java.util.List;

import agent.AbstractDeltaAgent;
import agent.abilities.Ability;
import agent.abilities.protocols.ObserverProtocol.R1_ReceiveUpdateNotification;
import agent.abilities.protocols.ObserverProtocol.R1_SendSubscribeObservableRequest;
import agent.abilities.protocols.ObserverProtocol.R2_ReceiveSubscriptionRequest;
import agent.abilities.protocols.ObserverProtocol.R2_ReceiveUnSubscriptionRequest;
import agent.abilities.protocols.ObserverProtocol.R2_SendUpdateNotification;
import debug.Debug;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import knowledge.LocationK;
import knowledge.ObservableK;

/**
 * <pre>
 * User									Agent
 * 
 * Send    -------- suicide ------------>	Receive
 * (Receive -------- ok      ------------>	Send)
 * This class allow any user to ask the agent to kill himself
 * 
 * This Ability currently only work within the platform.
 * \TODO If the agent is not there, forward to the GK that will find him
 *</pre>
 */

//DEBUG SET= 6

public class P_Suicide implements Ability{

	/************************************************************************************
	 * 
	 * 									Sender's role 
	 *
	 ************************************************************************************/

	/**
	 * Only work on local platform, should use lastKnownLocation instead
	 * @author hc
	 *
	 */
	public class R1_SendSuicideRequest extends OneShotBehaviour {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2644102479936831714L;

		String targetName;
		LocationK lastKnownlocation;

		/**
		 * 
		 * @param a the agent requesting the killing
		 * @param agentName the targeted agent
		 * @param lastKnownLocation in order to send the message to the associated platform (null means local)
		 */
		public R1_SendSuicideRequest(Agent a,String agentName,LocationK lastKnownLocation) {
			super(a);
			targetName=agentName;
			lastKnownlocation=lastKnownLocation;
		}


		@Override
		public void action() {

			ACLMessage msg=new ACLMessage(ACLMessage.REQUEST);
			msg.setLanguage(MyOntology.LANGUAGE);
			msg.setOntology(MyOntology.ONTOLOGY_PROTO_SUICIDE);
			msg.setProtocol(MyOntology.PROTO_SUICIDE);

			msg.setSender(this.myAgent.getAID());
			msg.addReceiver(new AID(targetName,AID.ISLOCALNAME));

			this.myAgent.send(msg);

		}

	}



	/************************************************************************************
	 * 
	 * 									Receiver's role 
	 *
	 ************************************************************************************/


	public class R2_ReceiveSuicideRequest extends OneShotBehaviour {

		private static final long serialVersionUID = 2882425367253682658L;

		public R2_ReceiveSuicideRequest(Agent a) {
			super(a);
		}

		@Override
		public void action() {

			// build the good template (filter of messages)
			final MessageTemplate perfTmplt = MessageTemplate.MatchPerformative(ACLMessage.REQUEST);
			final MessageTemplate langTmplt = MessageTemplate.MatchLanguage(MyOntology.LANGUAGE);
			final MessageTemplate ontoTmplt = MessageTemplate.MatchOntology(MyOntology.ONTOLOGY_PROTO_SUICIDE);
			final MessageTemplate protTmplt = MessageTemplate.MatchProtocol(MyOntology.PROTO_SUICIDE);

			final MessageTemplate totalTmplt = MessageTemplate.and(MessageTemplate.and(MessageTemplate.and(perfTmplt, langTmplt),ontoTmplt),protTmplt);

			// try to get the message (if it cross the filter)
			final ACLMessage msg = super.myAgent.receive(totalTmplt);

			if (msg != null) {
				Debug.info(this.getClass(),this.myAgent.getLocalName()+ " must die, request from: " + msg.getSender(),6);
				super.myAgent.doDelete();
			} else {
					block();
			}

		}
	}


	@Override
	public List<Behaviour> getBehaviours(String role, List<List<Object>> protocolParameters) {
		List<Behaviour> lb=new ArrayList<Behaviour>();

		switch (role) {
		case MyOntology.PROTO_SUICIDE_ROLE_KILLER :
			//the alternative is to use the generic callMethod that use introspection (tools.GenericCall)
			lb.add(new R1_SendSuicideRequest((Agent)protocolParameters.get(0).get(0), (String)protocolParameters.get(0).get(1),(LocationK) protocolParameters.get(0).get(2)));
			break;
		case MyOntology.PROTO_SUICIDE_ROLE_KILLED:
			lb.add(new R2_ReceiveSuicideRequest((Agent)protocolParameters.get(1).get(0)));
			break;
		default :
			System.out.println("Suicide protocol -- getBehaviours -- unknown role: "+role);
		}
		
		return lb;
	}


}
