/**
 * 
 */
package agent.abilities;

import java.util.List;

import jade.core.behaviours.*;

/**
 * @author hc
 *
 */
public interface Ability {

	/**
	 * 
	 * @param role The role of the agent in this protocol
	 * @param behavioursParameters A protocol is composed of several behaviours, each one having several parameters
	 * @return The list of behaviours that correspond to the protocol for this specific role.
	 */
	public List<Behaviour> getBehaviours(String role,List<List<Object>> behavioursParameters);


	
}
