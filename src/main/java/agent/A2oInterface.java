package agent;

import jade.core.Agent;

/**
* This interface is used to expose some of the agents methods to the outside.
* Indeed in Jade the agents are meant to interact with the world by communicating or by using directly the availables methods
* on a received object.
* If its okay in most case, a pb appears when an object is shared among several agents.
* Indeed, in that case there is few solutions :
*  - a ref to the shared object is given to the agents at creation through the setup's  args
*  - an agent is used as a resource manager and is the interface of the object. The pb is that the agents then have to send an asynchronous message
*  to use the object, which is conceptually wrong in my mind.
* 
* The first solution is perfect but only work if the ref is given at the agent creation. Thus if the object appears 
* online or if the agent move on different computers, it is not possible.
* Our solution here is to consider at ONE time the agent as an object to give him a ref to the shared object. 
* 
* See the GateKeeper for an illustrative example
* 
* @author hc
*
*/

public interface A2oInterface {
	
	/**
	 * To give a ref to a shared object to the agent (the environment, when the agent arrives on a given platform)
	 * @param key the id of the object (its name when referring to the environment)
	 * @param o the object to share
	 * @return true is the object has been successfully added
	 */
	public boolean addSharableObject(String key, Object o);
	
//	/**
//	 * 
//	 * @return A ref to the agent, in order for the environment to be able to kill him 
//	 */
//	//TOO use the AMS to retrieve it.
//	public Agent getSelfRef();

}
