package agent;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import agent.abilities.Ability;
import knowledge.Knowledge;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;

import dataStructures.*;

/**
 * This class offers a generic agent architecture for the DELTA project allowing to : 
 * <ul>
 * <li> Manipulate the various behaviours that compose a protocol as a set (an ability)</li>
 * <li> Sharing knowledge between behaviours of an agent, if they know their existence</li>
 * <li> Manage the entry/exit of agents through gatekeepers</li>
 * </ul>
 * @author hc
 *
 */
public abstract class AbstractDeltaAgent extends Agent implements A2oInterface,Serializable {

	private static final long serialVersionUID = 3802266615818490953L;

	/**
	 * Associate to each abilityID its knowledge DB (the necessary attributes and methods)
	 * 
	 */
	private MapOfMap<String, String,Knowledge> kDB;

	/**
	 * Associate an objectId to an Object  
	 */
	private Map<String, Object> sharedObjects;
	
	
	public AbstractDeltaAgent() {
		super();
		registerO2AInterface(A2oInterface.class,this);
		kDB=new MapOfMap<String, String, Knowledge>();
		sharedObjects= new HashMap<String,Object>();
	}

	/**
	 * Add the knowledge element k to the agent knowledge DB and associate it to a given functionality (protocol,behaviour,....)
	 * @param abilityID
	 * @param k
	 * @return the conversationID
	 */
	public String setKnowledge(String abilityID,Knowledge k){
		return this.kDB.add(abilityID, k);
	}

	/**
	 * 
	 * @param abilityID
	 * @param conversationID
	 * @return the knowledge element associated to the couple abilityID - conversationID 
	 */
	public Knowledge getKnowledge(String abilityID,String conversationID){
		Knowledge k= this.kDB.get(abilityID,conversationID);
		return k;
	}

	/**
	 * 
	 * @param abilityID
	 * @return the knowledge element associated to the ability ID
	 * ONLY for behaviours with a single possible instance, not for protocols.
	 */
	public Knowledge getKnowledge(String abilityID){
		Knowledge k= this.kDB.get(abilityID,abilityID+"-0");
		return k;
	}

	/**
	 * Add the object to the shared ones, only if the key does not already exist (replace otherwise)
	 * @return true if add successful
	 */
	public boolean  addSharableObject(String key, Object o){
		if (sharedObjects.containsKey(key)){
			return false;
		}else{
			sharedObjects.put(key, o);
		}
		return true;
	}
	
	
	public Object getSharableObject(String key){
		return this.sharedObjects.get(key);
	}

	public void removeSharableObject(String key){
		sharedObjects.remove(key);
	}
	
	
	/**
	 * In order to add a protocol to an agent you need
	 * @param ability an instance of the class that describes the ability (or protocol) in order to be able to get the associated behaviours through the call getBehaviours(role) method
	 * @param abilityID the unique ID of the specific protocol used by the agents when they communicate 
	 * @param role the role of the agent (e.g. Observer/Observable)
	 * @param behavioursParameters for each behaviour associated to the protocol, its parameters
	 * @param knowledge the knowledge component associated to this protocol
	 * <br/>
	 *  -e.g-
	 * <br/>
	 * <ul> 
	 *  <li>addAbility(ConsensusProtocol,PaxosID, proposer-role,list of agents, Knowledge component)</li> 
	 *  <li>addAbility(ObserverProtocol,ElementObservedID,observer-role,behavioursParameters,K) : note that abilityId is used here to identify bot the observer protocol and what is observed</li>
	 *  <li>addAbility(ObserverProtocol,ElemenyObservedID,observable-role,behavioursParameters,Knowledge component allowing the observable to keep track of its observers)</li>
	 *  <li>addAbility(CoffeeMachine,ID,user-role)</li>
	 *  <li>addAbility(CoffeeMachine,ID,machine-role)</li>
	 *  </ul>
	 * An alternative to the use of an instance of the protocol class is to use the genericCall class and introspection
	 */

	public void addAbility(Ability ability,String abilityID,String role,List<List<Object>> behavioursParameters,Knowledge knowledge){
		//Call to the protocol class that will get me all the behaviours necessary to add to the agent regarding the role r

		//specific knowledge component required for the protocol
		String conversationID=this.setKnowledge(abilityID, knowledge);

		//the conversationID must be given to the behaviours. Even if they may not use it
		for (List<Object> lo:behavioursParameters){
			lo.add(conversationID);
		}

		List<Behaviour> lb=ability.getBehaviours(role,behavioursParameters);
		for (Behaviour b:lb){
			this.addBehaviour(b);
		}
	}

	//	public void addAbility(Behaviour b,String abilityId,List<Object> behaviourParameters,Knowledge k){
	//		String conversationID=this.setKnowledge(abilityId, k);
	//		behaviourParameters.add(conversationID);
	//		this.addBehaviour(b)
	//		
	//	}



	//	/**
	//	 * 
	//	 * @param AbilityName the class name for now 
	//	 * @param specificId
	//	 * @param role
	//	 * @param parameters
	//	 */
	//	public void addAbility(String abilityName,String specificId,String role,List<List<Object>>parameters){
	//		//Call to the protocol class that will get me all the behaviours necessary to add to the agent regarding the role r
	//
	//		//create an instance of the generic class that contains the behaviours for the differents roles of this ability
	//		Ability generic= GenericCall.callMethod(MyOntology.getFullPath(abilityName),abilityName,null , null,null, true);
	//		
	//		//specific knowledge component required for the ability
	//		this.setKnowledge(specificId, generic.getKnowledge());
	//
	//		 
	//		List<Behaviour> lb=generic.getBehaviours(role,parameters);
	//		for (Behaviour b:lb){
	//			this.addBehaviour(b);
	//		}
	//	}
	//	
	//	/**
	//	 * 
	//	 * @param AbilityName the class name for now 
	//	 * @param specificId
	//	 * @param role
	//	 */
	//	public void addAbility(String abilityName,String specificId,String role){
	//		//Call to the protocol class that will get me all the behaviours necessary to add to the agent regarding the role r
	//
	//		//create an instance of the generic class that contains the behaviours for the differents roles of this ability
	//		Ability generic= GenericCall.callMethod(MyOntology.getFullPath(abilityName),abilityName,null , null,null, true);
	//		
	//		//specific knowledge component required for the ability
	//		this.setKnowledge(specificId, generic.getKnowledge());
	//
	//		 
	//		List<Behaviour> lb=generic.getBehaviours(role);
	//		for (Behaviour b:lb){
	//			this.addBehaviour(b);
	//		}
	//	}
	//	
	//	
	//	public void addAbility(String abilityName,String specificId,String role,Knowledge k){
	//		//Call to the protocol class that will get me all the behaviours necessary to add to the agent regarding the role r
	//
	//		//create an instance of the generic class that contains the behaviours for the differents roles of this ability
	//		Ability generic= GenericCall.callMethod(MyOntology.getFullPath(abilityName),abilityName,null , null,null, true);
	//		
	//		//specific knowledge component required for the ability
	//		this.setKnowledge(specificId, k);
	//
	//		 
	//		List<Behaviour> lb=generic.getBehaviours(role,parameters);
	//		for (Behaviour b:lb){
	//			this.addBehaviour(b);
	//		}
	//	}





}
