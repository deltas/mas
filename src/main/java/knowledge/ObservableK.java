/**
 * 
 */
package knowledge;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.List;


/**
 * Abstract class of knowledge for the observer pattern.
 * Observable elements are provided with an observerList
 * @author hc.
 *
 */
public class ObservableK extends Knowledge {

	private static final long serialVersionUID = -3143185018082643880L;
	
	/**
	 * The list of agents that want to be kept updated. 
	 */
	private List<AID> observersList;
	
	private Knowledge specificData;
	
	
	/*************************************
	 *         
	 *           CONSTRUCTOR(S)
	 * 
	 ************************************/
		
//	public GenericObservableK() {
//		super();
//		observersList=new ArrayList<AID>();
//		specificData=null;
//	}
	
	/**
	 * Create an observable element from a given knowledge k by providing him a list a observers and the associated methods
	 * @param k
	 */
	public ObservableK(Knowledge k) {
		super();
		observersList=new ArrayList<AID>();
		specificData=k;
	}
	
	/*************************************
	 *         
	 *           METHOD(S)
	 * 
	 ************************************/
	
	/**
	 * Add the observerAgent as an observer regarding the protocolId
	 * @param observerAgent
	 */
	public void addObserver(AID observerAgent){
		observersList.add(observerAgent);
	}


	/**
	 * Remove the observerAgent as an observer for the protocolId
	 * @param protocolId
	 * @param observerAgent
	 */
	public void removeObserver(AID observerAgent) {
		observersList.remove(observerAgent);
	}
	
	/**
	 * 
	 * @param protocolID
	 * @return the AID of the Agents that observe the observable agent for the information that correspond to protocolID.
	 * return null if no observer.
	 */
	public List<AID> getObservers(String protocolID){
		return this.observersList;
	}


	
	/**********************************************
	 * 
	 * 
	 * 				ENCAPSULATION
	 * 
	 * 
	 *******************************************/
	
	
	
	
	


	@Override
	public void update(Agent ag) {
		this.specificData.update(ag);
		
	}

	
	@Override
	public void updateK(ACLMessage msg) {
		this.specificData.updateK(msg);
		
	}



	/**
	 * Used by the observable agent.
	 * @return the object containing all the data needed by the observer's Agents regarding the protocol protocolId
	 */
	@Override
	public Knowledge getPublicData(Agent ag) {
		return this.specificData.getPublicData(ag);
	}
	
	public String toString(){
		return ""+this.getClass()+ "\n Observer list: "+this.observersList+ "\n --" +this.specificData.toString();
	}
	

}
