/**
 * 
 */
package knowledge;

import java.io.Serializable;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;

/**
 * Generic class of the knowledge components manipulated by the agents
 * @author hc
 *
 */
public abstract class Knowledge implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3084947283233421185L;
	//protected Agent ag;

	/*************************************
	 *         
	 *           CONSTRUCTOR(S)
	 * 
	 ************************************/
	
	public Knowledge(){
		//System.out.println("Ceci est un test pour la s�rialisation, normalement ne doit jamais etre appell�e car pas de classe non s�rialisable dans le extends");
	}

//	public Knowledge(Agent ag) {
//		this.ag=ag;
//	}

	
	/*************************************
	 *         
	 *           METHOD(S)
	 * 
	 ************************************/
	
	/**
	 * For a given knowledge component, it collects the needed informations and updates its state 
	 */	
	public abstract void update(Agent ag);
	

/**
 * A knowledge component updates its state using the informations stored in the message m.
 * Usually used by an agent that receives a message and wants to update its own knowledge with new informations
 * @param s
 */
	public abstract void updateK(ACLMessage msg);
	
	/**
	 * Used to extract from a knowledge component the data to share
	 * @param ag 
	 * @return A knowledge object that contain only the informations that the agent is willing to share over the network to another agent. Null if there is nothing to share
	 */
	public abstract Knowledge getPublicData(Agent ag);

	/*************************************
	 *         
	 *           GET and SET
	 * 
	 ************************************/
	
	public abstract String toString();
}
