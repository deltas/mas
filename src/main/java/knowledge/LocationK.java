package knowledge;

import jade.core.Agent;
import jade.lang.acl.ACLMessage;

/**
 * <pre>
 * Location object.
 * It contains PlatformName,IP and Port
 * </pre>
 * 
 * @author hc
 *
 */
public class LocationK extends Knowledge {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1984678894653364210L;
	private String homePlatformName;
	private String homeIP;
	private String homePort;
	
	public LocationK(String homePlatformName,String homeIP,String homePort) {
		this.homePlatformName=homePlatformName;
		this.homeIP=homeIP;
		this.homePort=homePort;
	}
	
	@Override
	public void update(Agent ag) {
		//Nothing as home cannot be changed

	}

	@Override
	public void updateK(ACLMessage msg) {
		//Nothing as home cannot be changed

	}

	@Override
	public Knowledge getPublicData(Agent ag) {
		return new LocationK(homePlatformName, homeIP, homePort);
	}

	@Override
	public String toString() {
		return "Home's platormName: "+this.homePlatformName+" - IP : "+this.homeIP+" - Port: "+this.homePort;
	}

}
